<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//los recursos en plurar si o si por el estandar rest.
//lo que este en {} son argumentos de esa ruta.
//{name?} la interrogación es un valor opcional.
//si no se comenta las otras funciones o no se pone delante de ellas. NO COGERA esa función.
//crear controladores con php artisan make::controler loloController
//Route::resource('users','UserController'); es equivalente a las 7 rutas de rest


Route::get('/', function () {
    return view('welcome');
});

Route::get('users',function(){
    return "Lista de usuarios";
});
/*
Route::get('users','UserController@index')->name('usuarios');
Route::get('users/{id}/edit','UserController@edit');
Route::get('users/create','UserController@create');*/
Route::resource('users','UserController');
// Route::get('users/especial','UserController');
/*

Route::get('users','UserController@index')->name('usuarios');
Route::get('users/create','UserController@create');
Route::get('users/{id}','UserController@show')->name('id');
*/
//Route::get('users/{id}',function ($id){
  //  return "Detalle del usuario $id";
//});


//Route::get('users/{id}/{name}',function ($id,$name){
 //   return "Detalle del usuario $id. El nombre es $name";
//});
/*
Route::get('users/{id}/{name?}',function ($id,$name=null){
    if($name){
         return "Detalle del usuario $id. El nombre es $name";
    }else{
         return "Detalle del usuario $id.Eres anónimo";
    }
});
*/
