<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function index()
    {
        $users = User::all();
       // return $users;
        //$users =array('juan','pepe','sandra','diego');
       // $users = array();
        return view('user.index',['users' => $users]);
        //busca el fichero : /resources/views/user/index.php o //resources/views/user/index.blade.php
    }

    public function create()
    {
        return view('user.create');
        //nota : en laravel las funciones incorporadas se denominan helpers , view es una de ellas.
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules = [
            'name' => 'required| max:255|min:5',
            'email' => 'required|unique:users|max:255|email',
            'paswword' => 'required|max:255',
             'color' => 'required|max:255',
        ];

        $request->validate($rules);

         $user = new User;
         $user->name = $request->input('name');
         $user->email = $request->input('email');
         $user->password = bcrypt($request->input('password'));
         $user->remember_token =str_random(10);
         $user->save();

      /*$user = new User();
      $user->fill($request->all());
      $user->save();
*/
      return redirect('/users');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

    // $user = User::find($id);
         return view('user.show', [
             'user'=> $user,
         ]);

       //  $otro ="otra variable.....";
       //  //echo"<pre>";
       // // dd(compact('id','otro'));
       //  return view('user.show',compact('id','otro'));
    }

    /**
     * Show the form for editing the specified resource.
     *3
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {



        $user = User::findOrFail($id);
        return view('user.edit',['user'=> $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $rules = [
            'name' => 'required| max:255|min:4',
            'email' => "required|unique:users,id,$id|max:255|email",
        ];

        $request->validate($rules);


      $user =User::findOrFail($id);
      $user->fill($request->all());
      $user->save();
     return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return back();
    }

    public function especial(){
        return redirect('/users');
        //return " especial ";
    }
}
