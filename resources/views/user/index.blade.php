<!DOCTYPE html>
<html>
<head>
    <title>Lista de Usuarios</title>
</head>
<body>
    <h1>Es la lista de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <ul>
    @forelse($users as $user)
        <li>{{ $user ->name}}: {{$user->email}} <a href="/users/{{$user->id}}/edit">editar </a>
            <form method="post" action="/users/{{ $user->id}}">
                {{csrf_field() }}
                <input type="hidden" name="_method" value="DELETE" >
                <input type="submit" name="borrar">
            </form>
        </li>
    @empty
        <li>NO hay usuarios </li>

    @endforelse
    </ul>
    <hr>

</body>
</html>
