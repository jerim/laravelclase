
    <h1>Alta de usuarios</h1>
    <style>
    .alert{
    padding: 10px;
    background-color: #f88;
    margin:5px;
    }

    </style>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{-- comentario de blade --}}
    <hr>
         @if ($errors->any())
            <div class="alert alert-danger">
                Se ha producido varios errores de validación
            </div>
         @endif

   <hr>


    <form method="post" action="/users">
        {{csrf_field()}}
        <label>Nombre</label>
        <input type="text" name="name" value="{{old('name')}}">
        <div class="alert alert-danger" >
            {{$errors->first('name')}}
        </div>

        <br>
         <label>Email</label>
        <input type="text" name="email" value="{{old('email')}}">
        <div class="alert alert-danger">
             {{$errors->first('email')}}
        </div>
         <br>
            <?php $example =['rojo', 'azul', 'verde'] ?>

            <select name="color">
                @foreach ($example as $color)
                    <option value="{{ $color }}" {!! old('color') == $color ? 'selected="selected"' : '' !!} >{{$color}}</option>
                @endforeach
            </select>
         <br>
         <label>Contraseña</label>
        <input type="text" name="password" >
        <div class="alert alert-danger">
             {{$errors->first('password')}}
        </div>
        <br>
        <input type="submit" name="enviar">
    </form>
