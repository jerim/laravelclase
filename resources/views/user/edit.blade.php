


    <h1>Editar usuarios</h1>
        <style>
    .alert{
    padding: 10px;
    background-color: #f88;
    margin:5px;
    }

    </style>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{-- comentario de blade --}}
    <hr>
         @if ($errors->any())
            <div class="alert alert-danger">
                Se ha producido varios errores de validación
            </div>
         @endif

   <hr>
    <form method="post" action="/users/{{
        $user->id }}">
        {{csrf_field()}}
         <input type="hidden" name="_method" value="PUT">
        <label>Nombre</label>
        <input type="text" name="name" value="{{old('name') ? old('name') : $user->name}}">
         <div class="alert alert-danger" >
            {{$errors->first('name')}}
        </div>

        <br>
         <label>Email</label>
        <input type="text" name="email"  value="{{old('email') ? old('email') : $user->email}}">
         <div class="alert alert-danger" >
            {{$errors->first('email')}}
        </div>

        <br>
        <input type="submit" name="enviar">
    </form>
