<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
   use RefreshDatabase;



   public function test_lista_de_usuarios_vacia()
    {

       $response= $this->get('/users');
       $response->assertStatus(200);
       $response->assertSee("Lista de Usuarios");

    }


     public function test_lista_de_usuarios()
     {
        factory(User::class)->create([
          'name' => 'pepe',
          'email' => 'pepe@gmail.com'
      ]);

       $response = $this->get('/users');
       $response->assertStatus(200);
       $response->assertSee("Lista de Usuarios");
       $response->assertSee('pepe');
       $response->assertSee("pepe@gmail.com");
    }


     public function test_metodo_show_user_1()
  {
      factory(User::class)->create([
          'id' => 1,
          'name' => 'pepe',
          'email' => 'pepe@gmail.com'
      ]);

      $response = $this->get('/users/1');
      $response->assertStatus(200);
      $response->assertSee('detalle del usuario 1');
      $response->assertSee('pepe');
      $response->assertSee('pepe@gmail.com');
  }

  public function test_metodo_show_user_inexistente()
  {
    $response = $this->get('/user/100000');
    $response->assertStatus(404);
  }

    //     public function test_metodo_show_user_1()
    // {
    //      $response= $this->get('/users/104');
    //      $response->assertStatus(200);
    //      $response->assertSee("detalle del usuario 104");


    //  }

}
